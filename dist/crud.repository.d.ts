import { Type } from '@nestjs/common';
import { BaseEntity } from './base-entity';
import { IRepository } from './repository';
declare type Constructor<I> = new (...args: any[]) => I;
export declare function CRUDRepository<T extends BaseEntity>(entity: Constructor<T>): Type<IRepository<T>>;
export {};
