import { Repository } from 'typeorm';
import { BaseEntity } from './base-entity';
import { Key } from './key.type';
export interface IRepository<E extends BaseEntity> {
    getRepository(): Repository<E>;
    getAll(): Promise<E[]>;
    get(id: Key): Promise<E | undefined>;
    update(entity: E): Promise<E | undefined>;
    create(entity: E): Promise<Key>;
    delete(id: Key): void;
}
