import { Key } from './key.type';
export declare class BaseEntity {
    uuid: Key;
    id: Key;
    createdAt: Date;
    modifiedAt: Date;
}
