import { Type } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { BaseEntity } from './base-entity';
import { IRepository } from './repository';

import { BadGatewayException } from '@nestjs/common';
import { Key } from './key.type';

type Constructor<I> = new (...args: any[]) => I;

export function CRUDRepository<T extends BaseEntity>(
  entity: Constructor<T>,
): Type<IRepository<T>> {
  class _CRUDRepository<E extends BaseEntity> implements IRepository<E> {
    constructor(@InjectRepository(entity) private repository: Repository<E>) {}

    getRepository(): Repository<E> {
      return this.repository;
    }

    async create(entity: any): Promise<Key> {
      try {
        var created = await this.repository.save(entity);
        return created.id;
      } catch (error) {
        throw new BadGatewayException(error);
      }
    }

    getAll(): Promise<E[]> {
      try {
        return <Promise<E[]>>this.repository.find();
      } catch (error) {
        throw new BadGatewayException(error);
      }
    }

    get(id: Key): Promise<E | undefined> {
      try {
        return this.repository.findOne(id);
      } catch (error) {
        throw new BadGatewayException(error);
      }
    }

    delete(id: Key) {
      try {
        this.repository.delete(id);
      } catch (error) {
        throw new BadGatewayException(error);
      }
    }

    async update(entity: E): Promise<E | undefined> {
      try {
        var found = this.get(entity.uuid);
        var newEntity: E = {
          ...found,
          ...entity,
        };
        this.repository.createQueryBuilder().update(entity.uuid, newEntity);

        return this.repository.findOne(entity.uuid);
      } catch (error) {
        throw new BadGatewayException(error);
      }
    }
  }
  return _CRUDRepository;
}
