import {
  Column,
  CreateDateColumn,
  Generated,
  Index,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Key } from './key.type';

export class BaseEntity {
  @PrimaryGeneratedColumn('uuid')
  public uuid: Key;

  @Index()
  @Generated('increment')
  public id: Key;

  @CreateDateColumn({ name: 'created_at' })
  public createdAt: Date;

  @UpdateDateColumn({ name: 'modified_at' })
  public modifiedAt: Date;
}
